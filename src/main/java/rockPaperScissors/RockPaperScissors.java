package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true){
        
        System.out.println("Let's play round"+" "+roundCounter);
        String userinput = readInput("Your choice (Rock/Paper/Scissors)? ");

        //Check if userinput is valid
        if(!userinput.equals("paper") && !userinput.equals("rock") && !userinput.equals("scissors")){
            System.out.println("I do not understand"+" "+userinput+". Could you try again?");
        }else{
                // Generate a random number from 0-3 and assign to List. Rock = 0, paper = 1 and scissors = 2
            int randomnum = (int)(Math.random() * 3);
            String computer = "";
            if(randomnum == 0) {
                computer = rpsChoices.get(0);
                //System.out.println(index_1);       
            } else if(randomnum ==1){
                computer = rpsChoices.get(1);
                //System.out.println(index_2);
            } else{
                computer = rpsChoices.get(2);
                //System.out.println(index_3);
            }

            //Calculate if the user won,lost or tied
            if(userinput.equals(computer)){
                //System.out.println("You tied!");
                System.out.println("Human chose"+" "+userinput+", computer chose"+" "+computer+". It's a tie!");
                roundCounter++;
            }else if(userinput.equals("rock") && computer.equals("scissors")){
                //System.out.println("You won!");
                System.out.println("Human chose"+" "+userinput+", computer chose"+" "+computer+". Human wins!");
                roundCounter++;
                humanScore++;

            }else if(userinput.equals("scissors") && computer.equals("paper")){
                //System.out.println("You won");
                System.out.println("Human chose"+" "+userinput+", computer chose"+" "+computer+". Human wins!");
                roundCounter++;
                humanScore++;
            }else if(userinput.equals("paper") && computer.equals("rock")){
                //System.out.println("You won!");
                System.out.println("Human chose"+" "+userinput+", computer chose"+" "+computer+". Human wins!");
                roundCounter++;
                humanScore++;
            }else{
                //System.out.println("You lost!");
                System.out.println("Human chose"+" "+userinput+", computer chose"+" "+computer+". Computer wins!");
                roundCounter++;
                computerScore++;
            }

            //Print score
            System.out.println("Score: human"+" "+humanScore+","+" computer"+" "+computerScore);

            //Ask player if they want to continue playing
            String play = readInput("Do you wish to continue playing? (y/n)?");
            if(play.equals("y")){
                continue;
            }
            if(play.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }

        }
        
        
        
        
    }
}
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
